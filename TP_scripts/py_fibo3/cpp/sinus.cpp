#include <math.h>
#include <pybind11/pybind11.h>

double Csin(double a, double b, double x){
	return sin(2*M_PI*(a*x+b));
}

PYBIND11_PLUGIN(sinus) {
    pybind11::module m("sinus");
    m.def("Csin", &Csin);
    return m.ptr();
}
