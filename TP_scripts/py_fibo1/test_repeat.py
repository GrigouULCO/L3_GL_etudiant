#! /usr/bin/env python3

import fibo
import repeat

def print_fibo(n):
	repeat.repeatN(n, fibo.fiboIterative)
