#include "Fibo.hpp"
#include <CppUTest/CommandLineTestRunner.h>

TEST_GROUP(FiboAssert) { };

TEST(FiboAssert,test_fibo_1){
    	int result = fibo(1);
	CHECK_EQUAL(result,1);
}

TEST(FiboAssert,test_fibo_2){
    	int result = fibo(2);
	CHECK_EQUAL(result,1);
}

TEST(FiboAssert,test_fibo_3){
    	int result = fibo(3);
	CHECK_EQUAL(result,2);
}

TEST(FiboAssert,test_fibo_4){
    	int result = fibo(4);
	CHECK_EQUAL(result,3);
}

TEST(FiboAssert,test_fibo_5){
    	int result = fibo(5);
	CHECK_EQUAL(result,5);
}

